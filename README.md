# Important

**IMPORTANT! This Minecraft Mod was made using MCreator, because I am still learning how to make a Minecraft Mod using well, actual code. This probabaly explains why the mod is pretty basic, and nothing game-changing. Sorry, I promise better mods are coming soon**

**Please read the README.md file so I don't get angry people joining my Discord and complaining that I didn't explain things properly**

# Soul Decor

## What is Soul Decor?

Soul Decor is a Minecraft Mod that adds several vanilla friendly Soul-Based mechanics into Minecraft. 
Features such as Soul-Based weaponary, new helpful (and potentially dangerous) items, potions, effects, and even mobs have been added, expanding on Minecraft. Yes, this mod was heavily inspired by servral other mods such as Arathain's [Mason Decor](https://www.curseforge.com/minecraft/mc-mods/mason-decor) mod (Thank you Arathain), and even some of [Simply Sword's](https://www.curseforge.com/minecraft/mc-mods/simply-swords) weapon's, like the Soulrender.

## Features 
- New Soul-Based weaponary, like a Scythe, which does something to the opposing player when they are at low health (No I won't spoil it for you)
- Another type of ore/material, creating new items and tools.
- More QOL items that aid the player through Mod progression 
- More unique mobs, some cute and harmless, some deadly, and defensive.
- New enchantments specifically designed for this Mod
- Mod is semi-balanced, as in the sense that you wont be able to go around one shotting player in full netherite (Cough cough Mace).

## Mod Guide

If you are lost! 

This section gives a quick guide on getting started, but if you really want the extended version of this section, check out the mod's [Wiki Page](https://gitlab.com/greater-end/minecraft-mods/soul-decor/-/wikis/Home-Page). 

Quick Guide: 
1. Scearch for a new block in the Nether. It generates in Soul Sand Valleys, and it is called "Soul Stone".
2. Soul Stone can only be mined with a iron pick or above, so stone and wooden tools will not work.
3. Upon mining, Soul Stone drops Essense, in quantities of 2-4, although this can be affected by Fortune.
4. Essense can be used in several crafting recipes, although the true purpose of essense is to be combined with other ingredients.
5. Essense can be combined with ingredients such as mushrooms, scute, feathers, and even several potions, to create other unique items
6. These unique items can then be used in their own crafting recipes, allowing you to progress further through the Mod.

Again, this section is not a complete guide to the Mod, but serves as a quick start guide. Please refer to the [Offical Wiki](https://gitlab.com/greater-end/minecraft-mods/soul-decor/-/wikis/Home-Page) for more information.

## Project Status/Roadmap

Uhh this Mod is more or less complete, I don't really have plans for adding more stuff. If I do have plans to add new stuff, I will probably place it at the top of this README file so yea. If you wish to add more stuff to this Mod, port it, or do funny stuff/configure it, please refer to the "Forking" part of this README.

# Technical Details
## Installation
This mod runs on Forge 1.20.1, and I have ran out of energy to update this Mod to the latets version. 

To install it simply create a new profile on the [Curse Forge launcher](https://www.curseforge.com/download/app), and ensure its on Forge, Minecraft version 1.20.1, otherwise you won't be able to locate the mod.

Upon locating the mod, simply press "Install", and the mod will be added to your profile/modpack. Simple enough right.

## Issue Tracker

Please report any issues in the GitLab repositories [Issue Tracker](https://gitlab.com/greater-end/minecraft-mods/soul-decor/-/issues) so I cn fix them. Not the Curse Forge Issue Tracker, but the GitLab one. Thanks.
## Conflicts
- Uhh please dont use optifine with this Mod. Pretty sure optifine inteferes with the game's base code, resulting in most mods not working with Optifine.

## Contributing

To contribute to this Project/Mod, please read the Contributing file that is in the GitLab repository.

## Credits
## Resources

- [MCreator](https://mcreator.net): The tool was used in making this mod, since i'm still figuring out how to use a JDK to make Minecraft Mods.
- [MCreator Wiki](https://mcreator.net/wiki): Uhh I definately refered to the wiki a lot, and as a result was able to make the Mod. Thanks wiki.
- [Curse Forge](https://www.curseforge.com/minecraft): Thanks Curse Forge for allowing my mod to be up there. Much appreciated.

## Inspiration

This Mod takes inspiration from a whole lot of other Mods so yeah. 

- Arathain's [Mason Decor](https://www.curseforge.com/minecraft/mc-mods/mason-decor) Mod
- Some of [Simply Swords](https://www.curseforge.com/minecraft/mc-mods/simply-swords) weapons
- Believe it or not, the [Souls](https://www.curseforge.com/minecraft/mc-mods/souls) mod as well
- The [Darker Souls](https://www.curseforge.com/minecraft/mc-mods/darker-souls) Mod, which actually took inspiration from the Dark Souls game.

If throughout updating and progressing this Mod I use anymore stuff or I have to credit people, I will list them here.

[![Tweeter](https://img.shields.io/badge/X-%23000000.svg?style=for-the-badge&logo=X&logoColor=white)](https://x.com/Skrillx_13)