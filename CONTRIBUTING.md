# Contributing
## Forking

If you wish to Fork this project, I have no obligations. Just click the button, Fork the project, get ahold of the source code, and edit it.

## Porting
Porting this Mod to Fabric, Bugrock, I mean Bedrock, or updating/backporting  this Mod to future versions? I have no obligations. However, please copy and paste the whole [README.md](https://gitlab.com/greater-end/minecraft-mods/soul-decor/-/blob/main/README.md?ref_type=heads#important) file, and put at the very top, under the Important section, that "**This Mod is a port/Backport/Updated Version of the original Soul Decor Mod.**", and include a link to my original project.

## Usage

Using this Mod in a Modpack? Go on ahead. Ypu can use this Mod 